<?php
ini_set('display_errors','on');
error_reporting(E_ALL);
require 'vendor/autoload.php';

use App\Http\Request;
use App\Router;
use Src\Controller\HomeController;
use Src\Controller\ConnexionController;
use Src\Controller\ArticleController;
use Src\Controller\PanelHomeController;
use Src\Controller\PanelEditArticleController;
use Src\Controller\AproposController;
use Src\Controller\ContactController;
use Src\Controller\MentionsLegalesController;

$request = new Request($_GET, $_POST, $_SERVER);

$router = new Router($request);

$liste = [
    ["/", HomeController::class, "index"],
    ["/a-propos", AproposController::class, "index"],
    ["/contact", ContactController::class, "index"],
    ["/deconnexion", ConnexionController::class, "deconnexion"],
    ["/mentions-legales", MentionsLegalesController::class, "index"],
    ["/connexion", ConnexionController::class, "index"],
    ["/ajax/connexion", ConnexionController::class, "ajax"],
    ["/article/", ArticleController::class, "index"],
    ["/panel/", PanelHomeController::class, "index"],
    ["/panel/connexion", ConnexionController::class, "index"],
    ["/ajax/createCommentaire", ArticleController::class, "createCommentaire"],
    ["/panel/edit/", PanelEditArticleController::class, "index"],
    ["/ajax/saveArticle", PanelEditArticleController::class, "saveArticle"],
    ["/ajax/uploadImage", PanelEditArticleController::class, "uploadImage"],
    ["/ajax/signaleCommentaire", ArticleController::class, "signaleCommentaire"],
    ["/ajax/contact", ContactController::class, "addMessage"],
    ["/ajax/deleteArticle", PanelHomeController::class, "deleteArticle"],
    ["/ajax/deleteMessage", PanelHomeController::class, "deleteMessage"],
    ["/ajax/deleteSignalement", PanelHomeController::class, "deleteSignalement"],
    ["/ajax/deleteSignalementAvecCommentaire", PanelHomeController::class, "deleteSignalementAvecCommentaire"],
];

$response = $router->run($liste);

$response->send();