<?php

namespace Src\Manager;

use App\Database;

class Message extends Database
{
    function getAllMessages()
    {
        $data = $this->bdd()->query("SELECT * FROM messages ORDER BY created DESC LIMIT 10");
        return $data;
    }
}
