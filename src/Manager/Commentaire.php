<?php

namespace Src\Manager;

use App\Database;

class Commentaire extends Database
{
    function getAllCommentairesOfArticle($id, $optionLimit)
    {
        if ($optionLimit == "Non défini") {
            $optionLimit = "0, 5";
        }
        $data = $this->bdd()->query("SELECT * FROM commentaires WHERE `id` = " . $id . " ORDER BY created DESC LIMIT " . $optionLimit);
        return $data;
    }

    function getCountOfCommentaires($id)
    {
        $data = $this->bdd()->prepare("SELECT COUNT(*) FROM commentaires WHERE `id` = " . $id . "");
        $data->execute();
        $data_resultat = $data->fetch();
        $data->closeCursor();
        return $data_resultat['COUNT(*)'];
    }

    function getAllSignalements()
    {
        $data = $this->bdd()->query("SELECT * FROM commentaires WHERE `statue` = 1");
        return $data;
    }
}
