<?php

namespace Src\Manager;

use App\Database;

class Article extends Database
{
    function getInfoArticle($id)
    {
        // $data = $this->bdd()->query("SELECT * FROM `articles` WHERE `id` = '" . $id . "'");
        // return $data;
        $data = $this->bdd()->prepare("SELECT * FROM `articles` WHERE `id` = '" . $id . "'");
        $data->execute();
        $data_resultat = $data->fetch();
        $data->closeCursor();
        return $data_resultat;
    }

    function getAllArticles()
    {
        $data = $this->bdd()->query("SELECT * FROM articles ORDER BY created DESC LIMIT 10");
        return $data;
    }

    function listeChapitre()
    {
        $data = $this->bdd()->query("SELECT categorie FROM articles GROUP BY categorie");
        return $data;
    }

    function getArticleHome($option)
    {
        if ($option == "Non défini") {
            $requet = "SELECT * FROM articles ORDER BY `type` DESC LIMIT 4";
        } else {
            $option = htmlspecialchars($option);
            if (is_numeric($option)) {
                $requet = "SELECT * FROM articles WHERE categorie LIKE '%" . $option . "%' ORDER BY created DESC LIMIT 4";
            } else {
                $requet = "SELECT * FROM articles WHERE nom LIKE '%" . $option . "%' OR  contenu LIKE '%" . $option . "%' OR  categorie LIKE '%" . $option . "%' ORDER BY created DESC LIMIT 4";
            }
        }
        $data = $this->bdd()->query($requet);
        return $data;
    }
}
