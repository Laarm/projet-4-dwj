<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;
use Src\Manager\Commentaire;
use Src\Manager\Message;

class PanelHomeController extends Controller
{
    public function index(): Response
    {
        $panelhome = new Article;
        $panelCommentaire = new Commentaire;
        $panelMessage = new Message;
        if (!$_SESSION['username']) {
            header('Location: /');
        } else {
            return $this->render("panel.home.html.twig", [
                "panelContent" => $this->request->getQueryParams("menu"),
                "username" => $_SESSION['username'],
                "articles" => $panelhome->getAllArticles(),
                "messages" => $panelMessage->getAllMessages(),
                "signalements" => $panelCommentaire->getAllSignalements(),
                "listeChapitre" => $panelhome->listeChapitre()
            ]);
        }
    }
    public function deleteArticle(): Response
    {
        if (!empty($_POST['id']) && isset($_SESSION['username'])) {
            $_POST['id'] = htmlspecialchars($_POST['id']);
            if ($this->bdd()->query("DELETE FROM `articles` WHERE id=" . $_POST['id'])) {
                return $this->json(['Success']);
            } else {
                return $this->json(['Failed']);
            }
        }
    }
    public function deleteMessage(): Response
    {
        if (!empty($_POST['id']) && isset($_SESSION['username'])) {
            $_POST['id'] = htmlspecialchars($_POST['id']);
            if ($this->bdd()->query("DELETE FROM `messages` WHERE id=" . $_POST['id'])) {
                return $this->json(['Success']);
            } else {
                return $this->json(['Failed']);
            }
        }
    }
    public function deleteSignalement(): Response
    {
        if (!empty($_POST['id']) && isset($_SESSION['username'])) {
            $_POST['id'] = htmlspecialchars($_POST['id']);
            if ($this->bdd()->query("UPDATE commentaires SET statue = '2' WHERE commentaireId = '" . $_POST['id'] . "'")) {
                return $this->json(["Success, Merci !"]);
            } else {
                echo $_POST['article_id'];
                return $this->json(["Failed, Erreur"]);
            }
        }
    }
    public function deleteSignalementAvecCommentaire(): Response
    {
        if (!empty($_POST['id']) && isset($_SESSION['username'])) {
            $_POST['id'] = htmlspecialchars($_POST['id']);
            $this->bdd()->query("UPDATE commentaires SET statue = '2' WHERE commentaireId = '" . $_POST['id'] . "'");
            if ($this->bdd()->query("DELETE FROM `commentaires` WHERE commentaireId=" . $_POST['id'])) {
                return $this->json(['Success']);
            } else {
                return $this->json(['Failed']);
            }
        }
    }
}
