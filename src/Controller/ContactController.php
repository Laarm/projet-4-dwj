<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;
use App\Database;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
class ContactController extends Controller
{
    public function index(): Response
    {
        $contact = new Article;
        return $this->render("contact.html.twig", [
            "username" => $_SESSION['username'],
            "listeChapitre" => $contact->listeChapitre()
        ]);
    }
    public function addMessage(): Response
    {
        $database = new Article;
        if (isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['telephone']) && isset($_POST['message'])) {
            $_POST['nom'] = htmlspecialchars($_POST['nom']);
            $_POST['email'] = htmlspecialchars($_POST['email']);
            $_POST['telephone'] = htmlspecialchars($_POST['telephone']);
            $_POST['message'] = htmlspecialchars($_POST['message']);
            if (!empty($_POST['nom']) && !empty($_POST['email']) && !empty($_POST['telephone']) && !empty($_POST['message'])) {
                if ($database->bdd()->query("INSERT INTO `messages` (`nom`, `email`, `telephone`, `message`, `created`) VALUES ('" . $_POST['nom'] . "', '" . $_POST['email'] . "', '" . $_POST['telephone'] . "', '" . $_POST['message'] . "', " . time() . ")")) {
                    $mail = new PHPMailer(true);
                    try {
                        //Server settings
                        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        //Recipients
                        $mail->Host       = "SSL0.OVH.NET";
                        $mail->SMTPDebug  = 0;
                        $mail->SMTPAuth   = true;
                        $mail->Host       = "SSL0.OVH.NET";
                        $mail->Port       = 587;
                        $mail->Username   = $database->emailEmail();
                        $mail->Password   = $database->emailMdp();
                        $mail->SetFrom($database->emailEmail(), 'Billet simple pour l\'Alaska - Administrateur');
                        $mail->AddReplyTo($database->emailEmail(), "Billet simple pour l\'Alaska - Administrateur");
                        $mail->addAddress('arnajacques@orange.fr');               // Name is optional
                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Nouveau message - Billet simple pour l\'Alaska';
                        $mail->Body    = 'Bonjour !</br> Vous avez reçu un nouveau message : <a href="http://localhost:8888/Projet%204%20-%20DWJ/index.php/contact">Cliquez ici</a>';
                        $mail->AltBody = 'Vous avez reçu un nouveau message : ' . $_SERVER['HTTP_REFERER'] . '/panel/';
                        $mail->send();
                        // echo 'Message has been sent';
                        return $this->json(['Success, Votre message à bien été envoyer, merci !']);
                    } catch (Exception $e) {
                        // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                        return $this->json(['Failed, Veuillez contacter un administrateur (arnajacques@orange.fr)']);
                    }
                } else {
                    return $this->json(['Failed, Veuillez contacter un administrateur (arnajacques@orange.fr)']);
                }
            } else {
                return $this->json(['Failed, Veuillez remplir tout les champs !']);
            }
        }
    }
}
