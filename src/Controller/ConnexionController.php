<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class ConnexionController extends Controller
{
    public function index(): Response
    {
        $connexion = new Article;
        if ($_SESSION['username']) {
            header('Location: /panel/');
        } else {
            return $this->render("connexion.html.twig", [
                "listeChapitre" => $connexion->listeChapitre()
            ]);
        }
    }

    public function ajax(): Response
    {
        // Chiffrement du mot de passe
        $_POST['password'] = htmlspecialchars($_POST['password']);
        $data = $this->bdd()->query("SELECT `username`, `password` FROM `users` WHERE `username` = '" . htmlspecialchars($_POST['username']) . "'");
        $data = $data->fetch();
        $username = $data['username'];
        $password = $data['password'];
        if (password_verify($_POST['password'], $data['password'])) {
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            return $this->json(['Success']);
        } else {
            return $this->json(['Failed']);
        }
    }
    public function deconnexion(): Response
    {
        $connexion = new Article;
        unset($_SESSION['username']);
        unset($_SESSION['passeword']);
        return $this->render("connexion.html.twig", [
            "listeChapitre" => $connexion->listeChapitre()
        ]);
    }
}
