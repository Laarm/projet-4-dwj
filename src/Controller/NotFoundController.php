<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class NotFoundController extends Controller
{
    public function index(): Response
    {
        $notfound = new Article;
        return $this->render("notfound.html.twig", [
            "username" => $_SESSION['username'],
            "listeChapitre" => $notfound->listeChapitre()
        ]);
    }
}
