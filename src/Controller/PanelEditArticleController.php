<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class PanelEditArticleController extends Controller
{
    public function index(): Response
    {
        $paneleditarticle = new Article;
        if (!$_SESSION['username']) {
            header('Location: ' . $this->infoWebsite("url"));
        } else {
            return $this->render("panel.editarticle.html.twig", [
                "username" => $_SESSION['username'],
                "article_id" => $this->request->getQueryParams("article"),
                "article" => $paneleditarticle->getInfoArticle(htmlspecialchars($this->request->getQueryParams("article"))),
                "listeChapitre" => $paneleditarticle->listeChapitre()
            ]);
        }
    }
    public function saveArticle(): Response
    {
        if (isset($_POST['nom']) && isset($_POST['contenu']) && isset($_POST['article_id']) && isset($_POST['categorie']) && isset($_SESSION['username'])) {
            $_POST['nom'] = htmlspecialchars($_POST['nom']);
            $_POST['article_id'] = htmlspecialchars($_POST['article_id']);
            $_POST['categorie'] = htmlspecialchars($_POST['categorie']);
            if (empty($_POST['image'])) {
                $_POST['image'] = "https://scontent-cdg2-1.cdninstagram.com/vp/23a0f75b8f3f1f8d4324fd331f2526f0/5E5FF4E8/t51.2885-15/e35/s1080x1080/71022418_387653261929539_2767454389404154771_n.jpg?_nc_ht=scontent-cdg2-1.cdninstagram.com&_nc_cat=103";
            } else {
                $_POST['image'] = htmlspecialchars($_POST['image']);
            }
            if ($_POST['article_id'] == "new") {
                $requet = "INSERT INTO `articles` (`nom`, `contenu`, `categorie`, `image`, `type`, `created`) VALUES ('" . $_POST['nom'] . "', '" . $_POST['contenu'] . "', '" . $_POST['categorie'] . "', '" . $_POST['image'] . "', 'public', '" . date("Y-m-d H:i:s", time()) . "')";
                $success = "L'article à bien été créer !";
            } else {
                $requet = "UPDATE articles SET nom = '" . $_POST['nom'] . "', contenu = '" . $_POST['contenu'] . "', categorie = '" . $_POST['categorie'] . "', `image` = '" . $_POST['image'] . "' WHERE id = " . $_POST['article_id'];
                $success = "L'article à bien été mis à jour !";
            }
            if (!empty($_POST['nom']) && !empty($_POST['article_id']) && !empty($_POST['categorie'])) {
                if ($this->bdd()->query($requet)) {
                    return $this->json(["Success, " . $success . ""]);
                } else {
                    return $this->json(["Failed, Veuillez contacter un administrateur !"]);
                }
            } else {
                return $this->json(["Failed, Veuillez remplir tout les champs !"]);
            }
        } else {
            return $this->json(["Failed, Erreur lors de la mise à jour de l'article..."]);
        }
    }
    public function uploadImage(): Response
    {
        $filename = $_FILES['file']['name'];
        $location = "public/img/upload/" . $filename;
        $uploadOk = 1;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
        $valid_extensions = array("jpg", "jpeg", "png");
        if (!in_array(strtolower($imageFileType), $valid_extensions)) {
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            return $this->json(['Failed']);
        } else {
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                echo $location;
                return $this->json([]);
            } else {
                var_dump($_FILES['file']['tmp_name']);
                return $this->json(['Failed']);
            }
        }
    }
}
