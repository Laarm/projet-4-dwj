<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;
use Src\Manager\Commentaire;

class ArticleController extends Controller
{
    public function index(): Response
    {
        $article = new Article;
        $commentaire = new Commentaire;
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $this->render("article.html.twig", [
            "username" => $_SESSION['username'],
            "article_id" => $this->request->getQueryParams("article"),
            "article" => $article->getInfoArticle(htmlspecialchars($this->request->getQueryParams("article"))),
            "articles" => $article->getAllArticles(),
            "commentaires" => $commentaire->getAllCommentairesOfArticle($this->request->getQueryParams("article"), $this->request->getQueryParams("optionLimit")),
            "countCommentaire" => $commentaire->getCountOfCommentaires($this->request->getQueryParams("article")),
            "listeChapitre" => $article->listeChapitre(),
            "longURL" => $actual_link
        ]);
    }
    public function createCommentaire(): Response
    {
        if (!empty($_POST['pseudo']) && !empty($_POST['commentaire']) && !empty($_POST['article_id'])) {
            $_POST['pseudo'] = htmlspecialchars($_POST['pseudo']);
            $_POST['commentaire'] = $_POST['commentaire'];
            $_POST['commentaire'] = str_replace("'", "˙", $_POST['commentaire']);
            $_POST['article_id'] = htmlspecialchars($_POST['article_id']);
            $data = $this->bdd()->prepare("SELECT `pseudo` FROM `commentaires` WHERE `pseudo` = '" . $_POST['pseudo'] . "' AND `contenu` = '" . $_POST['commentaire'] . "' AND `id` = '" . $_POST['article_id'] . "' LIMIT 1");
            $data->execute();
            $data_resultat = $data->fetch();
            $data->closeCursor();
            $data = $this->bdd()->prepare("INSERT INTO `commentaires` (`id`,`pseudo`,`contenu`,`created`) VALUES (" . $_POST['article_id'] . ", '" . $_POST['pseudo'] . "', '" . $_POST['commentaire'] . "', '" . date("Y-m-d H:i:s", time()) . "')");
            if (!isset($data_resultat["pseudo"])) {
                if ($data->execute()) {
                    return $this->json(["Success, Vous commentaire à bien été ajouté !"]);
                } else {
                    return $this->json(["Failed, Veuillez contacter un administrateur !"]);
                }
            } else {
                return $this->json(["Failed, Vous avez déjà envoyer ce commentaire !"]);
            }
        } else {
            return $this->json(["Failed, Veuillez remplir tout les champs !"]);
        }
    }
    public function signaleCommentaire(): Response
    {
        if (isset($_POST['commentaireId'])) {
            $_POST['commentaireId'] = htmlspecialchars($_POST['commentaireId']);
            if ($this->bdd()->query("UPDATE commentaires SET statue = '1' WHERE commentaireId = '" . $_POST['commentaireId'] . "'")) {
                return $this->json(["Success, Merci !"]);
            } else {
                echo $_POST['article_id'];
                return $this->json(["Failed, Erreur"]);
            }
        } else {
            return $this->json(["Failed, Erreur"]);
        }
    }
}
