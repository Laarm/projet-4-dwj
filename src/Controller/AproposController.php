<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class AproposController extends Controller
{
    public function index(): Response
    {
        $apropos = new Article;
        return $this->render("apropos.html.twig", [
            "username" => $_SESSION['username'],
            "listeChapitre" => $apropos->listeChapitre()
        ]);
    }
}
