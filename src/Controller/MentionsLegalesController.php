<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class MentionsLegalesController extends Controller
{
    public function index(): Response
    {
        $mentionslegales = new Article;
        return $this->render("mentionslegales.html.twig", [
            "username" => $_SESSION['username'],
            "listeChapitre" => $mentionslegales->listeChapitre()
        ]);
    }
}
