<?php

namespace Src\Controller;

use App\Controller;
use App\Http\Response;
use Src\Manager\Article;

class HomeController extends Controller
{
    public function index(): Response
    {
        $home = new Article;
        return $this->render("home.html.twig", [
            "articles" => $home->getAllArticles(),
            "articles_tendance" => $home->getArticleHome($this->request->getQueryParams("search")),
            "username" => $_SESSION['username'],
            "listeChapitre" => $home->listeChapitre()
        ]);
    }
}
