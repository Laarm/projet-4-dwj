<?php

namespace App;

use App\Database;
use App\Http\HtmlResponse;
use App\Http\JsonResponse;
use App\Http\Request;

abstract class Controller extends Database
{

    protected $request;

    public function __construct(Request $request)
    {
        session_start();
        ini_set('display_errors', 'off');
        date_default_timezone_set('Europe/Paris');
        $this->request = $request;
    }

    public function render(string $view, array $data = []): HtmlResponse
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates/');
        $twig = new \Twig\Environment($loader);
        // $twig = new \Twig\Environment($loader, ['debug' => true]);
        // $twig->addExtension(new \Twig\Extension\DebugExtension());
        $reducContent = new \Twig\TwigFunction('reducContent', function ($content, $max) {
            $content = strip_tags($content);
            return substr($content, 0, $max);
        });
        $twig->addFunction($reducContent);

        return new HtmlResponse($twig->render($view, $data));
    }

    public function json(array $data): JsonResponse
    {
        return new JsonResponse($data);
    }
}
