<?php

namespace App;

use PDO;

abstract class Database
{
    protected function bdd() {
		try{
			return new PDO('mysql:host=IPDELABASE;dbname=NOMDELABASE;charset=utf8', 'IDENTIFIANT', "MOTDEPASSE");
		}catch (Exception $e){
			die('Erreur : ' . $e->getMessage());
		}
	}
    protected function emailEmail() {
		return "EMAIL";
	}
    protected function emailMdp() {
		return "MOTDEPASSE";
	}
}