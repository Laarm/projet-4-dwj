<?php

namespace App;

use App\Http\Request;
use App\Http\Response;
use Src\Controller\NotFoundController;

class Router
{

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function run($liste): Response
    {
        foreach($liste as list($path, $class, $method))
        {
            if($this->request->getUri() === $path) {
                $controller = new $class($this->request);
                return $controller->$method();
            }
        }
        $controller = new NotFoundController($this->request);
        return $controller->index();
    }
}