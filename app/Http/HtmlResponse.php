<?php

namespace App\Http;

class HtmlResponse extends Response
{
    private $html;

    public function __construct(string $html)
    {
        $this->html = $html;
    }
    public function send(): void
    {
        echo $this->html;
    }
}