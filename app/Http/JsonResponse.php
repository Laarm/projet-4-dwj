<?php

namespace App\Http;

// class JsonResponse extends Response
class JsonResponse extends Response
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function send(): void
    {
        echo json_encode($this->data,JSON_FORCE_OBJECT|JSON_UNESCAPED_UNICODE);
    }
}