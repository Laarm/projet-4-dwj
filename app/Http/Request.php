<?php

namespace App\Http;

class Request {
    private $queryParams = [];
    private $postParams = [];
    private $uri = [];
    private $method = [];

    /*
    *   Request constructor.
    *   @params array $get
    *   @params array $post
    *   @params array $server
    */
    public function __construct(array $get, array $post, array $server)
    {
        $this->queryParams = $get;
        $this->postParams = $post;
        $this->uri = $server["PATH_INFO"] ?? "/";
        $this->method = $server["REQUEST_METHOD"];
    }

    public function getQueryParams(?string $key = null)
    {
        if(!empty($this->queryParams[$key])){
            return $this->queryParams[$key];
        }else{
            return "Non défini";
        }
    }
    public function getPostParams(): array
    {
        return $this->postParams;
    }
    public function getUri(): string
    {
        return $this->uri;
    }
    public function getMethod(): string
    {
        return $this->method;
    }
}