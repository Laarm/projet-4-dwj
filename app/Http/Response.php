<?php

namespace App\Http;

abstract class Response
{
    abstract public function send(): void;
}