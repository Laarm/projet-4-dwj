<?php

namespace App\Http;

class RedirectResponse extends Response
{
    private $uri;

    public function __construct(string $uri)
    {
        $this->uri = $uri;
    }
    public function send(): void
    {
        header("location".$this->uri);
    }
}